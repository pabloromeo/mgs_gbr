// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MGSGBGameMode.generated.h"

UCLASS(minimalapi)
class AMGSGBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMGSGBGameMode();
};



