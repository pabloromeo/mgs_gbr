// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MGSGB : ModuleRules
{
	public MGSGB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" });
	}
}
